import { createRouter, createWebHistory } from "vue-router";

export const routes = {
  Landing: "Landing",
  Detail: "Detail",
};

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/:zoom?/:lat?/:lng?",
      name: "Landing",
      alias: "/:zoom/:lat/:lng",
      component: () => import("../views/LandingView.vue"),
    },
    {
      path: "/detail",
      name: "Detail",
      component: () => import("../views/DetailView.vue"),
    },
    {
      path: "/land",
      name: "Land",
      component: () => import("../views/LandView.vue"),
    },
  ],
});

export default router;
