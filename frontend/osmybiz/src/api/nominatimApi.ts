import axios from "axios";
import { latLng } from "leaflet";
import * as _ from "lodash";
import { nominatimReverseUrl, nominatimUrl } from "../config/config.js";
import { useErrorStore } from "@/stores/error.js";
import type { Address } from "@/types/types.js";

const queryMax = 10;

function parseCoords(lat: string, lng: string) {
  const latNr = parseFloat(lat);
  const lngNr = parseFloat(lng);

  if (Number.isNaN(latNr) || Number.isNaN(lngNr)) {
    return null;
  }

  return latLng(latNr, lngNr);
}

function parseAddress(data: any): Address {
  const street =
    data.pedestrian || data.road || data.suburb || data.footway || "";
  const place = data.place || "";
  const housenumber = data.house_number || "";
  const postcode = data.postcode || "";
  const city = data.city || data.village || data.town || data.hamlet || "";
  const country = data.country || "";

  return {
    street,
    housenumber,
    place,
    postcode,
    city,
    country,
  };
}

function mapResults(results: any) {
  const points = results
    .map((point: any) => ({
      coords: parseCoords(point.lat, point.lon),
      osmId: point.osm_id,
      address: parseAddress(point.address),
    }))
    .filter((p: any) => _.isObject(p.coords) && p.address.city !== undefined);
  if (points.length === 5) {
    return points;
  }
  return points.splice(0, 5);
}

function buildRequest(q: string, count: number, language: string): string {
  const c = count > queryMax ? queryMax : count;
  return `${nominatimUrl}?format=json&q=${q}&limit=${c}&addressdetails=1&accept-language=${language}`;
}

export async function query(
  queryString: string,
  language: string
): Promise<any> {
  try {
    const response = await axios.get(buildRequest(queryString, 7, language));
    return mapResults(response.data);
  } catch {
    const errorStore = useErrorStore();

    errorStore.setError({ errorMessageKey: "error.nominatim" });
  }
}

function buildReverseRequest(position: { lat: number; lng: number }): string {
  return `${nominatimReverseUrl}?format=json&lat=${position.lat}&lon=${position.lng}&addressdetails=1&zoom=18`;
}

export async function reverseQuery(position: {
  lat: number;
  lng: number;
}): Promise<any> {
  try {
    const response = await axios.get(buildReverseRequest(position));
    if (response.data.error === "Unable to geocode") {
      return "";
    }
    return parseAddress(response.data.address);
  } catch {
    const errorStore = useErrorStore();

    errorStore.setError({ errorMessageKey: "error.nominatim" });
  }
}
