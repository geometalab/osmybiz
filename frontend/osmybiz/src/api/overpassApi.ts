import axios, { type AxiosResponse } from "axios";
import { lru } from "tiny-lru";
import { overpassUrl, searchradius } from "../config/config.js";
import tags from "../assets/tags/de.json";
import { useErrorStore } from "@/stores/error.js";
import type { Details, OverpassElement, Bbox } from "@/types/types.js";
import { isUndefined } from "lodash";

export const categoryTags = ["shop", "amenity", "tourism", "office", "leisure"];

const tagRegex = categoryTags.join("|");

const query = `[out:json];nwr[~"^${tagRegex}$"~"."]({{bbox}});out center;`;
const surroundingQuery = `[out:json];node(around:${searchradius}, {{lat}}, {{lon}})[{{tag}}={{cat}}]["name"="{{name}}"];out;`;

const millidegreesPerTile = 10; // results in 1km^2 tiles

const cache = lru<Array<OverpassElement>>(25);
const executing_queries: { [key: string]: Promise<AxiosResponse<any>> | undefined } = {};

function buildQuery(bbox: AlignedBbox) {
  return query.replace(
    "{{bbox}}",
    `${bbox.south / 1000}, ${bbox.west / 1000}, ${bbox.north / 1000}, ${
      bbox.east / 1000
    }`
  );
}

function buildSurroundingQuery(details: Details, lat: number, lon: number) {
  return surroundingQuery
    .replace("{{name}}", `${details.name}`)
    .replace("{{lat}}", `${lat}`)
    .replace("{{lon}}", `${lon}`)
    .replace("{{tag}}", `${details.category.value.split("/")[0]}`)
    .replace("{{cat}}", `${details.category.value.split("/")[1]}`);
}

type OverpassElementRaw = {
  id: number;
  lat: number;
  lon: number;
  center?: {
    lat: number;
    lon: number;
  };
  tags: {
    [key: string]: any;
  };
  type: string;
};

class Tile {
  north: number;
  east: number;
  constructor(north: number, east: number) {
    this.north = north;
    this.east = east;
  }
  getKey(): string {
    return this.north + "/" + this.east;
  }

  shouldContain(element: OverpassElement) {
    return (
      element.lat * 1000 <= this.north &&
      element.lat * 1000 > this.north - millidegreesPerTile &&
      element.lng * 1000 <= this.east &&
      element.lng * 1000 > this.east - millidegreesPerTile
    );
  }
}

class AlignedBbox {
  south: number;
  west: number;
  north: number;
  east: number;

  static FromBbox(bbox: Bbox): AlignedBbox {
    return new AlignedBbox(
      upperTileLimit(bbox.north * 1000),
      upperTileLimit(bbox.east * 1000),
      lowerTileLimit(bbox.south * 1000),
      lowerTileLimit(bbox.west * 1000)
    );
  }

  constructor(north: number, east: number, south: number, west: number) {
    this.north = north;
    this.east = east;
    this.south = south;
    this.west = west;
  }

  getKey(): string {
    return `${this.north}/${this.east}/${this.south}/${this.west}`;
  }

  getTiles(): Tile[] {
    const verticalTiles = Math.round(
      (this.north - this.south) / millidegreesPerTile
    );
    const horizontalTiles = Math.round(
      (this.east - this.west) / millidegreesPerTile
    );

    const keys: Tile[] = [];
    for (let i = 0; i < verticalTiles; i++) {
      for (let j = 0; j < horizontalTiles; j++) {
        const north = this.north - i * millidegreesPerTile;
        const east = this.east - j * millidegreesPerTile;
        keys.push(new Tile(north, east));
      }
    }
    return keys;
  }
}

// basically map lon to lng for further use on our side
function parseData(
  elements: Array<OverpassElementRaw>
): Array<OverpassElement> {
  return elements.map((e: OverpassElementRaw) => {
    if (e.type !== "node") {
      // on ways there is a center object, although also lat and lon are defined
      e.lat = e.center!.lat;
      e.lon = e.center!.lon;
    }
    return {
      type: e.type,
      id: e.id,
      lat: e.lat,
      lng: e.lon,
      tags: e.tags,
    };
  });
}

// checks tags in Overpass Request result, if valid and returns them if in our tag list
function filterTags(overpassElement: OverpassElement) {
  return Object.keys(tags).some((f) => {
    const element = f.split("/");
    const key = element[0];
    const value = element[1];
    if (overpassElement.tags[key]) {
      if (overpassElement.tags[key].indexOf(value) === 0) {
        return true;
      }
    }
    return false;
  });
}

function upperTileLimit(value: number): number {
  return Math.ceil(value / millidegreesPerTile) * millidegreesPerTile;
}

function lowerTileLimit(value: number): number {
  return Math.floor(value / millidegreesPerTile) * millidegreesPerTile;
}

export async function queryBox(bbox: Bbox) {
  try {
    const alignedBbox = AlignedBbox.FromBbox(bbox);
    const unloadedTiles = determineUnloadedTiles(alignedBbox);
    await loadTiles(unloadedTiles);

    const elements: OverpassElement[][] = [];
    for (const tile of alignedBbox.getTiles()) {
      const tileElements = cache.get(tile.getKey()) ?? [];
      elements.push(tileElements?.filter((x) => IsElementInBBox(x, bbox)));
    }
    return elements.flat();
  } catch {
    const errorStore = useErrorStore();
    errorStore.setError({ errorMessageKey: "error.overpass.query" });
    return [];
  }
}

function determineUnloadedTiles(bbox: AlignedBbox): Tile[] {
  const loadedTiles = cache.keys();
  const tilesToDisplay = bbox.getTiles();

  return tilesToDisplay.filter((x) => !loadedTiles.includes(x.getKey()));
}

async function loadTiles(tiles: Tile[]) {
  if (tiles.length == 0) return;

  const north = Math.max(...tiles.map((t) => t.north));
  const south = Math.min(...tiles.map((t) => t.north)) - millidegreesPerTile;
  const east = Math.max(...tiles.map((t) => t.east));
  const west = Math.min(...tiles.map((t) => t.east)) - millidegreesPerTile;

  const box = new AlignedBbox(north, east, south, west);

  // Since we're using async, we can send multiple request at the same time
  // But we don't want to send the same requests multiple times
  // So we use executing_queries to check whether there's an existing request and await it's promise

  let promise = executing_queries[box.getKey()];

  if (isUndefined(promise)) {
    promise =  axios.post(overpassUrl, buildQuery(box));
    executing_queries[box.getKey()] = promise;
  }
  
  let res = await promise;

  if (!hasIncomingDataBeenProcessed(tiles)) {
    processIncomingData(tiles, res)
    executing_queries[box.getKey()] = undefined;
  }
  
}

function hasIncomingDataBeenProcessed(tiles: Tile[]): boolean {
  // if all the requested tiles are in the cache, they have been processed already
  for (const tile of tiles) {
    if (cache.get(tile.getKey()) == undefined) {
      return false;
    }
  }
  return true;
}

function processIncomingData(tiles: Tile[], response: AxiosResponse<any>) {
  
  const data = parseData(response.data.elements).filter(filterTags);

      for (const tile of tiles) {
        const tileData = data.filter((x) => tile.shouldContain(x));
        cache.set(tile.getKey(), tileData);
      }
}

function IsElementInBBox(element: OverpassElement, bbox: Bbox): boolean {
  return (
    element.lat <= bbox.north &&
    element.lat >= bbox.south &&
    element.lng <= bbox.east &&
    element.lng >= bbox.west
  );
}

export async function surroundingQueryBusinessPOI(
  details: Details,
  lat: number,
  lon: number
): Promise<boolean> {
  try {
    const res = await axios.post(
      overpassUrl,
      buildSurroundingQuery(details, lat, lon)
    );
    return res.data.elements.length > 0;
  } catch {
    const errorStore = useErrorStore();

    errorStore.setError({ errorMessageKey: "error.overpass.surrounding" });
    return false;
  }
}
