// @ts-ignore: this is how it's in the docs, but gives error nevertheless, works though
import { osmAuth } from "osm-auth";
import axios from "axios";
import {
  osmUrl,
  osmApiLevel,
  searchradius,
} from "../config/config.js";
import util from "../util/osmApiUtils.js";
import type { BusinessPOI, OsmUser, POI } from "@/types/types.js";
import { useErrorStore } from "@/stores/error.js";
const createNotePath = `${osmApiLevel}notes.json`;
const notePath = `${osmApiLevel}notes/`;
const createChangesetPath = `${osmApiLevel}changeset/create`;
const uploadChangesetPath = `${osmApiLevel}changeset/`;
const closeChangesetPath = `${osmApiLevel}changeset/`;
const userPath = `${osmApiLevel}user/details.json`;
const apiPath = `${osmApiLevel}`;

type Note = {
  text: string;
  lat: string;
  lon: string;
};

const auth = osmAuth({
  client_id: import.meta.env.VITE_CLIENT_ID,
  client_secret: import.meta.env.VITE_CLIENT_SECRET,
  scope: 'read_prefs write_api write_notes',
  redirect_uri: import.meta.env.VITE_REDIRECT_URI,
  url: osmUrl,
});

export async function login(): Promise<void> {

  // OSMAuth doesn't seem to do Promises, so we have to wrap it in one
  return new Promise((resolve, reject) => {
    auth.authenticate((err: any, res: any) => {
      if (err || !res) {
        const errorStore = useErrorStore();
        errorStore.setError({ errorMessageKey: "error.osm.login" });
        reject(err);
      }
      else {
        resolve();
      }
    });
  });
}

export function isLoggedInOSM(): boolean {
  return auth.authenticated();
}

export function setOauthToken(token: string): Promise<boolean> {
  return new Promise((resolve) => {
    auth.bootstrapToken(token, () => {
      resolve(true);
    });
  });
}

export function logoutUser(): void {
  auth.logout();
}

export function loadOsmUser(): Promise<{}> | Promise<OsmUser> {
  const errorStore = useErrorStore();

  return new Promise((resolve) => {
    if (!isLoggedInOSM()) {
      resolve({});
    } else {
      auth.xhr(
        { method: "GET", path: userPath },
        (err: Error, response: any) => {
          if (err) {
            errorStore.setError({ errorMessageKey: "error.osm.loadUser" });
            resolve({});
            return;
          }
          resolve(util.parseUser(response));
        }
      );
    }
  });
}

// default option for content-type/accept is application/json, which makes the rest of the code not work anymore
const options = {
  headers: { "content-type": "text/xml", Accept: "text/xml" },
};
// temporary fix to redirect to live api, because dev environment is currently broken
async function getBusinessPOI2(osmType: string, osmId: number): Promise<POI> {
  const url = `https://www.openstreetmap.org/api/0.6/${osmType}/${osmId}`;
  try {
    const res = await axios.get(url, options);
    return util.parseBusinessPOI(res.data, osmType);
  } catch {
    throw new Error("There was an error with getting the BPOI");
  }
}

export async function getBusinessPOI(
  osmType: string,
  osmId: number
): Promise<POI | null> {
  return new Promise((resolve, reject) => {
    // this still returns an error in the console when it can't find the element... can't catch it
    auth.xhr(
      {
        method: "GET",
        path: `${apiPath}${osmType}/${osmId}`,
      },
      async (
        err: { status: number },
        response: { osmType: string }
      ) => {
        if (err) {
          if (err.status === 404) {
            const res = await getBusinessPOI2(osmType, osmId);
            res.id ? resolve(res) : reject(err);
          } else {
            const errorStore = useErrorStore();

            errorStore.setError({ errorMessageKey: "error.osm.load" });
            reject(err);
          }
        } else {
          resolve(
            util.parseBusinessPOI(response, response.osmType)
          );
        }
      }
    );
  });
}

function closeChangeset(changesetId: string) {
  auth.xhr(
    {
      method: "PUT",
      path: `${closeChangesetPath + changesetId}/close`,
    },
    (err: Error) => {
      if (err) {
        const errorStore = useErrorStore();

        errorStore.setError({ errorMessageKey: "error.osm.load" });
      }
    }
  );
}

function uploadChangeset(node: any, changesetId: string) {
  const upload = util.constructUpload(node, changesetId);
  return new Promise((resolve) => {
    auth.xhr(
      {
        method: "POST",
        path: `${uploadChangesetPath + changesetId}/upload`,
        content: upload,
        options: {
          header: {
            "Content-Type": "text/xml",
          },
        },
      },
      (err: Error, response: any) => {
        if (err) {
          const errorStore = useErrorStore();

          errorStore.setError({ errorMessageKey: "error.osm.load" });
          resolve(null);
        } else {
          closeChangeset(changesetId);
          resolve(getBusinessPOI("node", util.extractId(response)));
        }
      }
    );
  });
}

export function postOsmNode(node: any) {
  const create =
    "<osm>" +
    "<changeset>" +
    '<tag k="comment" v="#OSMyBiz"/>' +
    '<tag k="created_by" v="OSMyBiz"/>' +
    '<tag k="changesets_count" v="1"/>' +
    "</changeset>" +
    "</osm>";
  return new Promise((resolve) => {
    auth.xhr(
      {
        method: "PUT",
        path: createChangesetPath,
        content: create,
        options: {
          header: {
            "Content-Type": "text/xml",
          },
        },
      },
      (err: any, changesetId: string) => {
        if (err) {
          const errorStore = useErrorStore();

          errorStore.setError({ errorMessageKey: "error.osm.postNode" });
        }
        resolve(uploadChangeset(node, changesetId));
      }
    );
  });
}

type PostNoteReturn = {
  html: string;
  text: string;
  id: number;
  link: string;
  status: string;
};
export function postOsmNote(note: Note): Promise<PostNoteReturn | null> {
  return new Promise((resolve) => {
    auth.xhr(
      {
        method: "POST",
        path: createNotePath,
        content: `lat=${note.lat}&lon=${note.lon}&text=${encodeURIComponent(
          note.text
        )}`,
      },
      (err: any, response: any) => {
        if (err) {
          const errorStore = useErrorStore();

          errorStore.setError({ errorMessageKey: "error.osm.postNote" });
          resolve(null);
        } else {
          const data = JSON.parse(response);
          resolve({
            html: data.properties.comments[0].html,
            text: data.properties.comments[0].text,
            id: data.properties.id,
            link: `${osmUrl}/note/${data.properties.id}/#map=19/${data.geometry.coordinates[1]}/${data.geometry.coordinates[0]}&layers=ND`,
            status: data.properties.status,
          });
        }
      }
    );
  });
}

export function reopenClosedNoteAndAddComment(
  note: Note,
  noteId: string
): Promise<PostNoteReturn | null> {
  return new Promise((resolve) => {
    auth.xhr(
      {
        method: "POST",
        path: `${notePath}${noteId}/reopen.json`,
        content: `text=${encodeURIComponent(note.text)}`,
      },
      (err: any, response: any) => {
        if (err) {
          const errorStore = useErrorStore();

          errorStore.setError({
            errorMessageKey: "error.osm.reopenClosedNoteAndAddComment",
          });
          resolve(null);
        } else {
          const data = JSON.parse(response);
          const mostRecentCommentIndex = data.properties.comments.length - 1;
          resolve({
            html: data.properties.comments[mostRecentCommentIndex].html,
            text: data.properties.comments[mostRecentCommentIndex].text,
            id: data.properties.id,
            link: `${osmUrl}/note/${noteId}/#map=19/${data.geometry.coordinates[1]}/${data.geometry.coordinates[0]}&layers=ND`,
            status: data.properties.status,
          });
        }
      }
    );
  });
}

export function postNoteAsComment(
  note: Note,
  noteId: string
): Promise<PostNoteReturn | null> {
  return new Promise((resolve) => {
    auth.xhr(
      {
        method: "POST",
        path: `${notePath}${noteId}/comment.json`,
        content: `text=${encodeURIComponent(note.text)}`,
      },
      (err: any, response: any) => {
        const noteIsClosed = 409;
        if (err) {
          if (err.status === noteIsClosed) {
            resolve(reopenClosedNoteAndAddComment(note, noteId));
          } else {
            const errorStore = useErrorStore();

            errorStore.setError({
              errorMessageKey: "error.osm.postNoteAsComment",
            });
            resolve(null);
          }
        } else {
          const data = JSON.parse(response);
          const mostRecentCommentIndex = data.properties.comments.length - 1;
          resolve({
            html: data.properties.comments[mostRecentCommentIndex].html,
            text: data.properties.comments[mostRecentCommentIndex].text,
            id: data.properties.id,
            link: `${osmUrl}/note/${noteId}/#map=19/${data.geometry.coordinates[1]}/${data.geometry.coordinates[0]}&layers=ND`,
            status: data.properties.status,
          });
        }
      }
    );
  });
}

export async function getNotes(lat: number, lng: number): Promise<any> {
  // 0.0001 lat equates to 11.1 meter
  // to get the accuracy of lng you have to multiply the distance with the cosinus of lat
  const distance = (0.0001 / 11.1) * searchradius;
  const left = lng + distance * Math.cos(lat);
  const bottom = lat - distance;
  const right = lng - distance * Math.cos(lat);
  const top = lat + distance;
  try {
    const response = await axios.get(
      `${osmUrl}${createNotePath}?bbox=${left},${bottom},${right},${top}`
    );
    return response.data.features;
  } catch {
    const errorStore = useErrorStore();

    errorStore.setError({ errorMessageKey: "error.osm.load" });
    return [];
  }
}

export async function getNotesByOsmId(noteId: number): Promise<any> {
  try {
    return await axios.get(`${osmUrl}${osmApiLevel}notes/${noteId}`, options);
  } catch {
    const errorStore = useErrorStore();

    errorStore.setError({ errorMessageKey: "error.osm.getNotesByOsmId" });
  }
}
