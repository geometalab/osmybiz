import type { BusinessPOI, Note, OsmUser, BPOI } from "@/types/types";
import * as _ from "lodash";

const userKey = "MOCK_API_USERS";
const noteKey = "MOCK_API_NOTES";

function saveUsers(users: Array<OsmUser>) {
  localStorage.setItem(userKey, JSON.stringify(users));
}

function saveBusinessPOIs(notes: Array<Note>) {
  localStorage.setItem(noteKey, JSON.stringify(notes));
}

function loadUsers() {
  return JSON.parse(localStorage.getItem(userKey) || "[]");
}

function loadBusinessPOIs() {
  return JSON.parse(localStorage.getItem(noteKey) || "[]");
}

export function mockAddOrUpdateUser(
  userId: number,
  displayName: string
): Promise<void> {
  const users = loadUsers();
  const existing: OsmUser = users.filter((u: OsmUser) => u.id === userId)[0];

  if (_.isObject(existing)) {
    existing.name = displayName;
  } else {
    users.push({ osmId: userId, name: displayName });
  }
  saveUsers(users);
  return Promise.resolve();
}

export function mockFetchBusinessPOIs(userId: number): Promise<any> {
  const users = loadUsers();
  const user = users.filter((u: OsmUser) => u.id === userId)[0];

  if (!_.isObject(user)) {
    return Promise.reject(new Error("User not found"));
  }

  const businessPOIs = loadBusinessPOIs().filter(
    (n: BusinessPOI) => n.userId === String(userId)
  );
  return Promise.resolve(businessPOIs);
}

export function mockAddOrUpdateBusinessPOI(
  userId: number,
  businessPOI: BPOI
): boolean {
  const users = loadUsers();
  const user = users.filter((u: OsmUser) => u.id === userId)[0];

  if (!_.isObject(user)) {
    return false;
  }
  const businessPOIs = loadBusinessPOIs();
  let existingBusinesPoi = businessPOIs.filter(
    (n: BPOI) => n.osmId === businessPOI.osmId && n.userId === userId.toString()
  )[0];
  if (!_.isObject(existingBusinesPoi)) {
    existingBusinesPoi = { osmId: businessPOI.osmId, userId };
    businessPOIs.push(existingBusinesPoi);
  }
  existingBusinesPoi.lat = businessPOI.lat;
  existingBusinesPoi.lng = businessPOI.lng;
  existingBusinesPoi.version = businessPOI.version;
  existingBusinesPoi.receiveUpdates = businessPOI.receiveUpdates;
  existingBusinesPoi.name = businessPOI.name;

  saveBusinessPOIs(businessPOIs);
  return true;
}

export function mockUnsubscribe(
  userId: number,
  businessPOIId: number
): Promise<void> {
  const users = loadUsers();
  const user = users.filter((u: OsmUser) => u.id === userId)[0];

  if (!_.isObject(user)) {
    return Promise.reject(new Error("User not found"));
  }
  const businessPOIs = loadBusinessPOIs();
  const businessPOI: BusinessPOI = businessPOIs.filter(
    (n: BusinessPOI) => n.osmId === businessPOIId && Number(n.userId) === userId
  )[0];
  if (!_.isObject(businessPOI)) {
    return Promise.reject(new Error("User not found"));
  }

  businessPOI.receiveUpdates = false;
  saveBusinessPOIs(businessPOIs);
  return Promise.resolve();
}

export function mockDeleteBusinessPOI(
  userId: string,
  businessPOIId: string
): Promise<void> {
  const users = loadUsers();
  const user = users.filter((u: OsmUser) => String(u.id) === userId)[0];

  if (!_.isObject(user)) {
    return Promise.reject(new Error("User not found"));
  }
  const businessPOIs = loadBusinessPOIs();
  const businessPOI = businessPOIs.filter(
    (n: BusinessPOI) =>
      n.osmId === parseInt(businessPOIId, 10) && n.userId === userId
  )[0];
  if (!_.isObject(businessPOI)) {
    return Promise.reject(new Error("User not found"));
  }

  const index = businessPOIs.indexOf(businessPOI);
  businessPOIs.splice(index, 1);
  saveBusinessPOIs(businessPOIs);
  return Promise.resolve();
}
