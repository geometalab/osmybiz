import { createI18n } from "vue-i18n";
import ar from "./ar.json";
import ca from "./ca.json";
import cs from "./cs.json";
import en from "@/locales/en.json";
import es from "./es.json";
import de from "@/locales/de.json";
import fa from "./fa.json";
import fr from "./fr.json";
import gl from "./gl.json";
import he from "./he.json";
import hu from "./hu.json";
import is from "./is.json";
import it from "./it.json";
import nl from "./nl.json";
import pl from "./pl.json";
import ru from "./ru.json";
import sv from "./sv.json";
import uk from "./uk.json";
import zh_TW from "./zh_TW.json";

const i18n = createI18n({
  legacy: false,
  locale: "en", // set locale
  fallbackLocale: "en", // set fallback locale
  messages: { ar, ca, cs, en, es, de, fa, fr, gl, he, hu, is, it, nl, pl, ru, sv, uk, zh_TW }, // set locale messages
});

export default i18n;
