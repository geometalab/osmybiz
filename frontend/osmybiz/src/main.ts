import { createApp } from "vue";
import { createPinia } from "pinia";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";

import {
  faWindowClose,
  faQuestionCircle,
  faExternalLink,
  faSync,
  faEnvelope,
  faPen,
  faBars,
  faPlusCircle,
  faEye,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";

import vSelect from "vue-select";
import i18n from "@/locales/i18n-config";

library.add(faWindowClose);
library.add(faQuestionCircle);
library.add(faExternalLink);
library.add(faSync);
library.add(faEnvelope);
library.add(faPen);
library.add(faBars);
library.add(faPlusCircle);
library.add(faEye);
library.add(faTimes);

import App from "./App.vue";
import router from "./router";

import "leaflet/dist/leaflet.css";
import "vue-select/dist/vue-select.css";

const app = createApp(App);
app.use(createPinia());
app.use(router);
app.use(i18n);
app.component("font-awesome-icon", FontAwesomeIcon);
app.component("v-select", vSelect);

import ErrorMessage from "./components/shared/ErrorMessage.vue";
import PostSuccess from "./components/landing/PostSuccess.vue";
import VBusinessMarkerPopup from "./components/map/VBusinessMarkerPopup.vue";
app.component("error-message", ErrorMessage);
app.component("post-success", PostSuccess);
app.component("v-business-marker-popup", VBusinessMarkerPopup);

app.mount("#app");
