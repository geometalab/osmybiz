import { categoryTags } from "../api/overpassApi";
import type { BusinessPOI, Details } from "@/types/types";

import { useLocaleStore } from "@/stores/locale";

function getBusinessPOICategoryKey(businessPOI: BusinessPOI) {
  let result = "";
  categoryTags.forEach((t: any) => {
    if (businessPOI.tags[t]) {
      result = `${t}/${businessPOI.tags[t]}`;
    }
  });
  return result;
}

type Option = {
  key: string;
  text: string;
};

type Field = {
  key: string;
  label: string;
  type: string;
  value: string;
  name?: string;
  options?: Array<Option>;
};
function getBizCategory(businessPOI: BusinessPOI): {
  text: string;
  fields: Array<Field>;
  value: string;
} {
  const localeStore = useLocaleStore();

  const key = getBusinessPOICategoryKey(businessPOI);
  const fields: Array<Field> = [];
  const tagFields = localeStore.getTagName(key).fields || [];
  tagFields.forEach((field: any) => {
    let value = "";
    if (businessPOI.tags[field.key]) {
      value = businessPOI.tags[field.key];
    }
    if (field.options) {
      const options: Array<Option> = [];
      Object.keys(field.options).forEach((option) => {
        options.push({
          key: option,
          text: field.options[option],
        });
      });
      fields.push({
        key: field.key,
        label: field.label,
        type: field.type,
        options,
        value,
      });
    } else {
      fields.push({
        key: field.key,
        label: field.label,
        type: field.type,
        value,
      });
    }
  });
  return {
    text: localeStore.getTagName(key).name, // localeStore.getTagName(key).name,
    fields,
    value: key,
  };
}

function extractTag(businessPOI: BusinessPOI, tagName: string) {
  return businessPOI.tags[tagName] || "";
}

// todo maybe rename to createDetails or similar
function createNoteFromBusinessPOI(businessPOI: BusinessPOI): Details {
  return {
    category: getBizCategory(businessPOI),
    name: extractTag(businessPOI, "name"),
    opening_hours: extractTag(businessPOI, "opening_hours"),
    opening_url: extractTag(businessPOI, "opening_hours:url"),
    phone: extractTag(businessPOI, "phone"),
    email: extractTag(businessPOI, "email"),
    website: extractTag(businessPOI, "website"),
    wheelchair: extractTag(businessPOI, "wheelchair"),
    description: extractTag(businessPOI, "description"),
    note: "",
  };
}

export { getBusinessPOICategoryKey, getBizCategory, createNoteFromBusinessPOI };
