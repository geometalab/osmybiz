export type BusinessPOI = {
  tags?: any;
  address?: any;
  details?: any;
  lat: number;
  lon?: number; // different naming between some things...
  lng: number;
  userId?: string;
  osmId: number;
  version: number;
  receiveUpdates: boolean;
  name: string;
  noteId: number | null;
  osmType: string;
};

export type POI = {
  id: number;
  lat: string;
  lon: string;
  link: string;
  address: object;
  details: Details;
  unknownTags: object;
  version: string;
  changeSet: string;
  tags: { name: string };
  noteIsResolved: boolean;
  hasUpdate: boolean;
  noteId: string;
};

export type SubscribedPOI = {
  lat: number;
  lng: number;
  hasUpdate: boolean;
  id: number;
  mine: boolean;
  noteId: number;
  noteIsResolved: boolean;
  tags: { name: string };
  type: string;
  version: number;
  warnings: Array<string>;
};

// hopefully the correct one...
export type BPOI = {
  osmId: number;
  noteId: number | null;
  userId: string;
  lat: number;
  lng: number;
  version: number;
  receiveUpdates: boolean;
  name: string;
  osmType: string;
};

export type Note = {
  id: number;
};

export type Address = {
  street: string;
  housenumber: string;
  place: string;
  postcode: string;
  city: string;
  country: string;
};

export type Details = {
  name: string;
  opening_hours: string;
  opening_hours_template?: OH_Details;
  opening_url: string;
  phone: string;
  email: string;
  website: string;
  wheelchair: string;
  description: string;
  note: string;
  category: Category;
};

export type OH_Details = Array<{
  id: number;
  isOpen: boolean;
  labeling: string;
  day: string;
  opensAt: [string, string, string];
  closesAt: [string, string, string];
  addTimes: [boolean, boolean];
}>;

export type Category = {
  text: string;
  value: string;
  fields?: Array<{ key: string; name?: string; value: string; label?: string }>;
};

export type SuccessMessage = {
  address: string;
  name: string;
  link: string;
  isNote: boolean;
};

export type OverpassElement = {
  id: number;
  lat: number;
  lng: number;
  tags: {
    [key: string]: any;
  };
  type: string;
};

export type LeafletMap = {
  center: { lat: number; lng: number };
  minZoom: number;
  maxZoom: number;
  zoom: number;
  bounds: Array<[number, number]>;
  maxBounds: Array<[number, number]>;
  leafletObject: any;
};

export type Suggestion = {
  address: Address;
  coords: { lat: number; lng: number };
  osmId: number;
};

export type Bbox = {
  south: number;
  west: number;
  north: number;
  east: number;
};

export type OsmUser = {
  id: number;
  langPrefs: Array<string>;
  name: string;
  unReadCount: number;
};

export type CurrentBPOI = {
  [key: string]: any;
  osmId: number;
  osmType: string;
  version: number;
  position: { lat: number; lng: number };
  lat: string;
  lon: string;

  address: Address;
  details: Details;

  isNew: boolean;
  isNote: boolean;

  noteId: string;
  note: {};
};
