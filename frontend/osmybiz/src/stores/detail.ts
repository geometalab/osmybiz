import deepEqual from "deep-equal";
import {
  postOsmNode,
  postOsmNote,
  getBusinessPOI,
  postNoteAsComment,
} from "../api/osmApi";
import { reverseQuery } from "../api/nominatimApi.js";
import {  getTemporaryOsmId } from "../api/osmybizApi.js";
import { MODIFIED, REMOVED } from "../config/config.js";

import type {
  Address,
  Details,
  SuccessMessage,
  POI,
  BPOI,
  OsmUser,
  CurrentBPOI,
} from "@/types/types";
import { useUserdialogStore } from "./userdialog";
import { useBPOIStore } from "./bpoi-store";
import { useUpdateStore } from "./update";

//const state = JSON.parse(JSON.stringify(initialState));
const emptyAddress = {
  street: "",
  housenumber: "",
  country: "",
  postcode: "",
  city: "",
  place: "",
};

export function createEmptyDetails(): Details {
  const details: Details = {
    category: {
      text: "",
      fields: [{ key: "", name: "", value: "", label: "" }],
      value: "",
    },
    name: "",
    opening_hours: "",
    opening_hours_template: [
      {id: 0, isOpen: false, labeling: "", day: "Mo ", opensAt: ["", "", ""], closesAt: ["", "", ""], addTimes: [false, false]},
      {id: 1, isOpen: false, labeling: "", day: "Tu ", opensAt: ["", "", ""], closesAt: ["", "", ""], addTimes: [false, false]},
      {id: 2, isOpen: false, labeling: "", day: "We ", opensAt: ["", "", ""], closesAt: ["", "", ""], addTimes: [false, false]},
      {id: 3, isOpen: false, labeling: "", day: "Th ", opensAt: ["", "", ""], closesAt: ["", "", ""], addTimes: [false, false]},
      {id: 4, isOpen: false, labeling: "", day: "Fr ", opensAt: ["", "", ""], closesAt: ["", "", ""], addTimes: [false, false]},
      {id: 5, isOpen: false, labeling: "", day: "Sa ", opensAt: ["", "", ""], closesAt: ["", "", ""], addTimes: [false, false]},
      {id: 6, isOpen: false, labeling: "", day: "Su ", opensAt: ["", "", ""], closesAt: ["", "", ""], addTimes: [false, false]},
      {id: 7, isOpen: false, labeling: "", day: "PH ", opensAt: ["", "", ""], closesAt: ["", "", ""], addTimes: [false, false]},
      {id: 8, isOpen: false, labeling: "", day: "24/7 ", opensAt: ["", "", ""], closesAt: ["", "", ""], addTimes: [false, false]},
    ],
    opening_url: "",
    phone: "",
    email: "",
    website: "",
    wheelchair: "",
    description: "",
    note: "",
  }

  return details;
};

import { ref, type Ref } from "vue";
import { defineStore } from "pinia";

const emptyBPOI = {
  osmId: -1,
  version: 0,
  osmType: "",

  position: { lat: 0, lng: 0 },
  lat: "",
  lon: "",

  address: emptyAddress,
  details: createEmptyDetails(),

  isNew: true,

  isNote: false,
  noteId: "",
  note: {},
};
export const useDetailStore = defineStore("detail", () => {
  const currentBPOI: Ref<CurrentBPOI> = ref(emptyBPOI);

  // separat
  const isPopup = ref(false);
  const infoText = ref("");
  const isFormSubmission = ref(false);
  const isEditingUnsavedChanges = ref(false);

  function setCoords(pos: { lat: number; lng: number }) {
    currentBPOI.value.position = pos;
    currentBPOI.value.lat = String(pos.lat);
    currentBPOI.value.lon = String(pos.lng);
  }

  function showPopup(text: string) {
    infoText.value = text;
    isPopup.value = true;
  }

  function hidePopup() {
    infoText.value = "";
    isPopup.value = false;
  }

  function resetDetailState() {
    // basically gotta reset everything to default state..
    // https://dev.to/the_one/pinia-how-to-reset-stores-created-with-functionsetup-syntax-1b74
  }
  function restoreDetailState() {
    const unsavedChanges = JSON.parse(
      localStorage.getItem("unsavedChanges") || ""
    );
    Object.keys(unsavedChanges).forEach((key: any) => {
      currentBPOI.value[key] = unsavedChanges[key];
    });
  }

  function clearDetails() {
    currentBPOI.value.details = createEmptyDetails();
  }

  async function postNode(user: OsmUser) {
    const businessPOI = {
      lat: currentBPOI.value.lat,
      lon: currentBPOI.value.lon,
      details: currentBPOI.value.details,
      address: currentBPOI.value.address,
      osmId: currentBPOI.value.osmId,
      osmType: currentBPOI.value.osmType,
      version: currentBPOI.value.version,
      unknownTags: currentBPOI.value.unknownTags,
    };
    const newlyCreatedNode = (await postOsmNode(businessPOI)) as {
      lat: string;
      lon: string;
      details: { name: string };
      id: string;
    };
    manageSuccessMessage(newlyCreatedNode, false);

    const updateStore = useUpdateStore();
    return updateStore.addOrUpdateBusiness(user.id, user.name, {
      lat: parseFloat(newlyCreatedNode.lat),
      lng: parseFloat(newlyCreatedNode.lon),
      name: newlyCreatedNode.details.name,
      noteId: null,
      osmId: parseInt(newlyCreatedNode.id, 10),
      osmType: "node",
      receiveUpdates: true,
      version: parseInt("0", 10),
      userId: String(user.id),
    });
  }

  async function getBPOIFromStoreIfAvailable(osmId: number) {
    const bpoiStore = useBPOIStore();
    currentBPOI.value = await bpoiStore.getBPOIIfStored(osmId);
  }

  function setBPOIToDefaultEmptyValues() {
    currentBPOI.value = emptyBPOI;
  }

  async function storeCurrentBPOIInStore() {
    const bpoiStore = useBPOIStore();
    await bpoiStore.storeBPOI(currentBPOI.value);
  }

  function manageSuccessMessage(note: any, isNote: boolean) {
    const userdialogStore = useUserdialogStore();

    const noteSuccessMessage = constructSuccessMessage(note, isNote);
    userdialogStore.successMessage = noteSuccessMessage;
  }
  async function postNote(
    user: OsmUser,
    osmId: number,
    noteId: string,
    osmType: string
  ) {
    let success;
    if (!noteId) {
      success = await postNoteWithoutNoteId(user, osmId, osmType);
    } else {
      success = await postNoteWithNoteId(user, noteId, osmType, osmId);
    }
    return success;
    // check how it's in the db, if as number
  }

  async function postNoteWithoutNoteId(
    user: OsmUser,
    osmId: number,
    osmType: string
  ): Promise<boolean> {
    const note = constructNote(false);
    const createdNote = await postOsmNote(note);
    if (!createdNote) {
      return false;
    }
    manageSuccessMessage(createdNote, true);
    const basePOI = {
      name: currentBPOI.value.details.name,
      noteId: createdNote.id,
      osmType: osmType,
      receiveUpdates: true,
      userId: user.id.toString(),
    };
    let somePOI = {} as BPOI;
    if (osmId) {
      const tempPOI = (await getBusinessPOI(osmType, osmId)) as POI | null;
      if (!tempPOI) {
        return false;
      }
      // lat & lng is undefined when it is a relation/way,
      // TODO calculate the lat lon instead of using the state
      somePOI = {
        ...basePOI,
        lat: tempPOI.lat
          ? parseFloat(tempPOI.lat)
          : parseFloat(currentBPOI.value.lat),
        lng: tempPOI.lon
          ? parseFloat(tempPOI.lon)
          : parseFloat(currentBPOI.value.lon),
        osmId: tempPOI.id,
        version: parseInt(tempPOI.version, 10),
      } as BPOI;
    } else {
      const temporaryOsmId: number | null = await getTemporaryOsmId(user.id.toString());
      somePOI = {
        ...basePOI,
        lat: parseFloat(currentBPOI.value.lat),
        lng: parseFloat(currentBPOI.value.lon),
        version: 0,
        osmId: temporaryOsmId,
      } as BPOI;
    }

    const updateStore = useUpdateStore();
    return updateStore.addOrUpdateBusiness(user.id, user.name, { ...somePOI });
  }

  async function postNoteWithNoteId(
    user: OsmUser,
    noteId: string,
    osmType: string,
    osmId: number
  ): Promise<boolean> {
    const note = constructNote(true);
    const noteThatWasSent = (await postNoteAsComment(note, noteId)) as {
      id: number;
    };
    manageSuccessMessage(noteThatWasSent, true);
    const basePOI = {
      name: currentBPOI.value.details.name,
      osmType: osmType,
      receiveUpdates: true,
      noteId: noteThatWasSent.id,
      userId: user.id.toString(),
    };
    let somePOI = {} as BPOI;
    if (osmType === "note") {
      somePOI = {
        ...basePOI,
        lat: parseFloat(currentBPOI.value.lat),
        lng: parseFloat(currentBPOI.value.lon),
        osmId: osmId,
        version: parseInt("0", 10),
      } as BPOI;
    } else {
      const businessPOI = (await getBusinessPOI(osmType, osmId)) as POI | null;
      if (!businessPOI) {
        return false;
      }
      somePOI = {
        ...basePOI,
        lat: businessPOI.lat
          ? parseFloat(businessPOI.lat)
          : parseFloat(currentBPOI.value.lat),
        lng: businessPOI.lon
          ? parseFloat(businessPOI.lon)
          : parseFloat(currentBPOI.value.lon),
        osmId: businessPOI.id,
        version: parseInt(businessPOI.version, 10),
      } as BPOI;
    }
    const updateStore = useUpdateStore();
    return updateStore.addOrUpdateBusiness(user.id, user.name, { ...somePOI });
  }

  async function getAddress(position: { lat: number; lng: number }) {
    // confusing naming IMHO, change maybe
    const createdAddress = (await reverseQuery(position)) as Address;
    // implement error handling
    currentBPOI.value.address = createdAddress;
    localStorage.setItem("address", JSON.stringify(currentBPOI.value.address));
  }

  async function getVersion(osmId: number, osmType: string): Promise<number> {
    const businessPOI = await getBusinessPOI(osmType, osmId);
    return parseInt(businessPOI?.version || "0");
  }

  function saveChangesTemporarily() {
    // somehw need to get all the data here and be able to save it...
    const unsavedChanges = JSON.stringify("");
    localStorage.setItem("unsavedChanges", unsavedChanges);
  }

  function getPrettifiedAddress() {
    const { street, housenumber, place, postcode, city, country } =
      currentBPOI.value.address;

    let prettifiedAddress = "";
    if (street) {
      prettifiedAddress += street;
      prettifiedAddress += housenumber ? ` ${housenumber},` : ",";
    }
    prettifiedAddress += place ? ` ${place},` : "";
    prettifiedAddress += postcode ? ` ${postcode}` : "";
    prettifiedAddress += city ? ` ${city}` : "";
    prettifiedAddress += country ? ` ${country}` : "";

    return prettifiedAddress;
  }

  function constructSuccessMessage(
    response: { link: string },
    isNote: boolean
  ): SuccessMessage {
    const { link } = response;
    const address = getPrettifiedAddress();
    const name = currentBPOI.value.details.name;
    const successMessage = {
      address,
      name,
      link,
      isNote,
    };
    return successMessage;
  }

  function constructNote(isNoteAsComment: boolean): {
    lat: string;
    lon: string;
    text: string;
  } {
    let text = "This is a comment generated by #OSMyBiz.\n";
    text += `New/Modified Tags = '${MODIFIED}', Removed Tags = '${REMOVED}'.\n \n`;

    text += constructAddrNote(currentBPOI.value.address);
    text += !isNoteAsComment ? "\n \n" : "\n";
    text += constructDetailNote(currentBPOI.value.details);
    text += !isNoteAsComment ? "\n \n" : "\n";
    text += constructCategoryNote(
      currentBPOI.value.details.category,
      currentBPOI.value.isOwnCategory
    );

    return {
      lat: currentBPOI.value.lat,
      lon: currentBPOI.value.lon,
      text,
    };
  }

  function isNotModified(): boolean {
    const details = JSON.parse(localStorage.getItem("details") || "");
    const address = JSON.parse(localStorage.getItem("address") || "");
    return (
      isDetailsEqual(details, currentBPOI.value.details) &&
      deepEqual(address, currentBPOI.value.address)
    );
  }

  function isDetailsEqual(details1: Details, details2: Details): boolean {
    return (
      details1.description === details2.description &&
      details1.email === details2.email &&
      details1.name === details2.name &&
      details1.note === details2.note &&
      details1.opening_hours === details2.opening_hours &&
      details1.opening_url === details2.opening_url &&
      details1.phone === details2.phone &&
      details1.website === details2.website
    );
  }

  return {
    currentBPOI,
    isFormSubmission,
    isEditingUnsavedChanges,
    isPopup,
    infoText,
    setCoords,
    showPopup,
    hidePopup,
    resetDetailState,
    restoreDetailState,
    clearDetails,
    postNode,
    manageSuccessMessage,
    postNote,
    getAddress,
    getVersion,
    saveChangesTemporarily,
    isNotModified,
    getBPOIFromStoreIfAvailable,
    setBPOIToDefaultEmptyValues,
    storeCurrentBPOIInStore,
    createEmptyDetails
  };
});

function parseTagToString(
  tag: string,
  value: any,
  initialValue: any,
  additionalText: string
): string {
  if (tag === "opening_url") {
    tag = "opening_hours:url";
  }
  if (deepEqual(value, initialValue)) {
    if (value) {
      return `${additionalText}${tag} = ${value}\n`;
    }
    return "";
  }
  if (value) {
    return `${MODIFIED}${additionalText}${tag} = ${value}\n`;
  }
  return `${REMOVED}${additionalText}${tag} = ${initialValue}\n`;
}

function constructAddrNote(address: any): string {
  let text = "";
  const originalAddress = JSON.parse(localStorage.getItem("address") || "");
  Object.keys(address).forEach((key) => {
    text += parseTagToString(key, address[key], originalAddress[key], "addr:");
  });
  return text;
}

function constructDetailNote(details: any): string {
  let text = "";
  const originalDetails = JSON.parse(localStorage.getItem("details") || "");
  Object.keys(details).forEach((key) => {
    if (key !== "category" && key !== "opening_hours_template") {
      text += parseTagToString(key, details[key], originalDetails[key], "");
    }
  });
  return text;
}

function constructCategoryNote(category: any, isOwnCategory: boolean): string {
  let text = "";
  const originalDetails = JSON.parse(localStorage.getItem("details") || "");
  const originalCategory = originalDetails.category;
  let categoryFormatted = "";

  if (isOwnCategory) {
    text += `${MODIFIED}category: ${category.text}\n`;
  } else {
    let field;
    if (category.value.indexOf("/") !== -1) {
      categoryFormatted = category.value.replace("/", " = ");
    }
    if (deepEqual(category.value, originalCategory.value)) {
      text += `${categoryFormatted}\n`;
      for (let i = 0; i < category.fields.length; i += 1) {
        // the field here is the additonal tag options that is dependant on the category
        field = category.fields[i];
        text += parseTagToString(
          field.key,
          field.value,
          originalCategory.fields[i].value,
          ""
        );
      }
    } else {
      text += `${MODIFIED}${categoryFormatted}\n`;
      for (let i = 0; i < category.fields.length; i += 1) {
        field = category.fields[i];
        if (field.value) {
          text += `${MODIFIED}${field.key} = ${field.value}\n`;
        }
      }
    }
  }
  return text;
}
