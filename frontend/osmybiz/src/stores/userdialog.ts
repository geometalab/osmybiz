import {
  NUM_OF_SECS_TO_SHOW_THE_UNSAVED_CHANGES_NOTIFICATION_DIALOG,
  NUM_OF_SECS_BEFORE_DELETING_THE_UNSAVED_CHANGES_DATA,
} from "../config/config";
import { ref } from "vue";
import type { Ref } from "vue";
import { defineStore } from "pinia";
import type { SuccessMessage } from "@/types/types";

const emptySuccessMessage = {
  address: "",
  name: "",
  link: "",
  isNote: false,
};

export const useUserdialogStore = defineStore("userdialog", () => {
  const successMessage: Ref<SuccessMessage> = ref(emptySuccessMessage);
  const displayUnsavedChangesNotifications = ref(false);
  const showDialogTimeLeft = ref(0);
  const timerId: Ref<NodeJS.Timer | undefined> = ref(undefined);

  function displayUnsavedChangesNotification() {
    clearInterval(timerId.value);
    displayUnsavedChangesNotifications.value = true;
    showDialogTimeLeft.value =
      NUM_OF_SECS_TO_SHOW_THE_UNSAVED_CHANGES_NOTIFICATION_DIALOG;
    timerId.value = setInterval(() => {
      showDialogTimeLeft.value -= 1;
      if (showDialogTimeLeft.value === 0) {
        displayUnsavedChangesNotifications.value = false;
      }
      if (
        showDialogTimeLeft.value ===
        -NUM_OF_SECS_BEFORE_DELETING_THE_UNSAVED_CHANGES_DATA
      ) {
        localStorage.removeItem("unsavedChanges");
        clearInterval(timerId.value);
      }
    }, 1000);
  }

  function clearSuccessMessage() {
    successMessage.value = emptySuccessMessage;
  }

  return {
    successMessage,
    displayUnsavedChangesNotifications,
    showDialogTimeLeft,
    timerId,
    displayUnsavedChangesNotification,
    clearSuccessMessage,
  };
});
