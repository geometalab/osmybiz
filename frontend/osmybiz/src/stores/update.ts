import * as _ from "lodash";
import {
  addOrUpdateUser,
  addOrUpdateBusinessPOI,
  fetchBusinessPOIs,
  unsubscribe,
} from "../api/osmybizApi.js";
import { getBusinessPOI, getNotesByOsmId } from "../api/osmApi";
import util from "../util/osmApiUtils.js";
import { useErrorStore } from "./error";
import { defineStore } from "pinia";
import { ref } from "vue";
import type { Ref } from "vue";
import type { OsmUser, POI, SubscribedPOI, BPOI } from "@/types/types.js";


export const useUpdateStore = defineStore("update", () => {
  const subscribedBusinessPOIs: Ref<Array<SubscribedPOI>> = ref([]);
  const showWatchList = ref(false);

  async function loadUpdates(userId: number, name: string): Promise<void> {
    await addOrUpdateUser(userId, name);
    const ns = await fetchBusinessPOIs(userId);

    subscribedBusinessPOIs.value = [];

    ns.filter((n: any) => n.receiveUpdates).forEach(
      async (n: any) => {
        const subscribedBusinessPOI: any = {
          id: n.osmId,
          lat: n.lat,
          lng: n.lng,
          mine: true,
          noteId: n.noteId,
          type: n.osmType,
          version: n.version,
          hasUpdate: false,
          noteIsResolved: false,
        };
        
        if (subscribedBusinessPOI.noteId !== null && subscribedBusinessPOI.noteId !== undefined) {
          const notes = await getNotesByOsmId(subscribedBusinessPOI.noteId);
          const noteStatus = util.parseNoteStatus(notes);
          if (noteStatus === "closed") {
            subscribedBusinessPOI.noteIsResolved = true;
          }
        }

        if (isNoteWithoutOsmElement(subscribedBusinessPOI.id)) {
          subscribedBusinessPOI.tags = {};
          subscribedBusinessPOI.tags.name = n.name;
          subscribedBusinessPOIs.value.push(subscribedBusinessPOI);
        } else {
          const osmBusinessPOI = await getBusinessPOI(n.osmType, n.osmId);
          if (_.isObject(osmBusinessPOI)) {
            subscribedBusinessPOI.tags = osmBusinessPOI.tags;
            if (hasVersionUpdate(subscribedBusinessPOI, osmBusinessPOI)) {
              subscribedBusinessPOI.hasUpdate = true;
            }

            subscribedBusinessPOI.warnings = runChecks(osmBusinessPOI);

            subscribedBusinessPOIs.value.push(subscribedBusinessPOI);
          } else {
            const errorStore = useErrorStore();

            errorStore.setError({
              errorMessageKey: "error.osm.osmElementDeleted",
              placeholders: [n.name],
            });
            subscribedBusinessPOI.tags = {};
            subscribedBusinessPOI.tags.name = n.name;
            subscribedBusinessPOIs.value.push(subscribedBusinessPOI);
          }
        }
      }
    );
  }

  /**
   * Adds or updates a business POI to the watchlist and reloads the watchlist.
   */
  async function addOrUpdateBusiness(userId: number, username: string, bpoi: BPOI ): Promise<boolean>{
    const success = await addOrUpdateBusinessPOI(userId, bpoi);

    if (success) {
      loadUpdates(userId, username);
    }

    return success;
  }

  async function removeFromWatchList(
    subscribedBusinessPOI: SubscribedPOI,
    user: OsmUser
  ): Promise<void> {
    await unsubscribe(user.id, subscribedBusinessPOI.id);
    loadUpdates(user.id, user.name);
  }

  return {
    subscribedBusinessPOIs,
    showWatchList,
    loadUpdates,
    addOrUpdateBusiness,
    removeFromWatchList,
  };
});

function hasVersionUpdate(
  subscribedBusinessPOI: SubscribedPOI,
  osmBusinessPOI: any
): boolean {
  return osmBusinessPOI.version > subscribedBusinessPOI.version;
}

function runChecks(poi: POI): string[] {
  let warnings: string[] = [];

  if (poi.details["opening_hours"] === undefined) {
    return warnings;
  }

  if ((<any>poi).details["check_date:opening_hours"] === undefined) {
    warnings.push("landing.watchlist.warnings.checkDateOHMissing");
  } else {
    const checkDate = Date.parse((<any>poi).details["check_date:opening_hours"]);
    const outdated = Date.now() - checkDate > 31536000000; // 1 year
    if (outdated) {
      warnings.push("landing.watchlist.warnings.checkDateOHOutdated");
    }
  }

  return warnings;
}

function isNoteWithoutOsmElement(osmId: number): boolean {
  return osmId < 0;
}
