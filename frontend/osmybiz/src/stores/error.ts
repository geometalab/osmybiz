import { defineStore } from "pinia";
import { ref } from "vue";
import type { Ref } from "vue";

export const useErrorStore = defineStore("error", () => {
  const errorMessageKey = ref<string>("");
  const placeholders: Ref<Array<string>> = ref([]);
  const isError = ref<boolean>(false);

  function setError(errorObj: {
    errorMessageKey: string;
    placeholders?: Array<string>;
  }) {
    errorMessageKey.value = errorObj.errorMessageKey;
    if (errorObj.placeholders) {
      placeholders.value = errorObj.placeholders;
    }
    isError.value = true;
    setTimeout(() => {
      isError.value = false;
    }, 5000);
  }

  return { errorMessageKey, placeholders, isError, setError };
});
