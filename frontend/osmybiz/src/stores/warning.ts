import { ref } from "vue";
import { defineStore } from "pinia";
import { surroundingQueryBusinessPOI } from "../api/overpassApi.js";
import { getNotes } from "../api/osmApi";
import { useDetailStore } from "./detail";
import type { BusinessPOI } from "@/types/types";

type Note = {
  properties: {
    id: string;
    status: string;
    comments: [{ text: string }];
  };
  geometry: { coordinates: Array<string> };
};

export const useWarningStore = defineStore("warning", () => {
  const isDuplicate = ref(false);
  const isConfirm = ref(false);
  const noteLink = ref("");

  async function checkDuplicateBusinessPOI(): Promise<boolean> {
    const detailStore = useDetailStore();

    // might be number instead of string (category.value) but seems to be string
    if (detailStore.currentBPOI.details.category.value === "") {
      isDuplicate.value = false;
      return false;
    }
    const isDuplicateBPOI = await surroundingQueryBusinessPOI(
      detailStore.currentBPOI.details,
      parseFloat(detailStore.currentBPOI.lat),
      parseFloat(detailStore.currentBPOI.lon)
    );
    isDuplicate.value = isDuplicateBPOI || false;
    return isDuplicate.value;
  }

  async function checkDuplicateNote(businessPOI: BusinessPOI): Promise<void> {
    const notes = await getNotes(businessPOI.lat, businessPOI.lng);
    let duplicate = false;
    let createdNoteLink = "";
    notes.forEach((note: Note) => {
      if (note.properties.status === "open") {
        const [tmp] = note.properties.comments;
        const { text } = tmp;
        const fields = text.split("\n");
        if (fields[0] === "#OSMyBiz ") {
          const cat = fields[3].split(":")[1].substring(1);
          if (
            fields[3] === `Category: ${cat}:${businessPOI.tags[cat]}` &&
            fields[4] === `Name: ${businessPOI.tags.name}`
          ) {
            duplicate = true;
            createdNoteLink = `https://master.apis.dev.openstreetmap.org/note/${note.properties.id}/#map=19/${note.geometry.coordinates[1]}/${note.geometry.coordinates[0]}&layers=ND`;
          }
        }
      }
    });
    noteLink.value = createdNoteLink;
    isDuplicate.value = duplicate;
  }

  return {
    isDuplicate,
    isConfirm,
    noteLink,
    checkDuplicateBusinessPOI,
    checkDuplicateNote,
  };
});
