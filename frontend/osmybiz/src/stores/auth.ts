import {
  login,
  setOauthToken,
  loadOsmUser,
  logoutUser,
  isLoggedInOSM,
} from "../api/osmApi";
import { defineStore } from "pinia";
import { ref } from "vue";
import type { OsmUser } from "@/types/types";

const emptyOsmUser: OsmUser = {
  id: 0,
  name: "",
  unReadCount: 0,
  langPrefs: [],
};

export const useAuthStore = defineStore("auth", () => {
  const isLoggedIn = ref(false);
  const user = ref(emptyOsmUser);

  async function authenticate(): Promise<void> {
    await login();
    await loadUser();
  }

  async function initAuth(): Promise<void> {
    if (!isLoggedInCheck()) {
      isLoggedIn.value = false;
      return;
    }
    isLoggedIn.value = true;
    await loadUser();
  }

  function isLoggedInCheck() {
    return isLoggedInOSM();
  }

  function setToken(token: string): Promise<boolean> {
    return setOauthToken(token);
  }

  async function loadUser(): Promise<void> {
    const osmUser = (await loadOsmUser()) as OsmUser;
    if (!osmUser) {
      isLoggedIn.value = false;
      throw new Error("There was a Problem with loading the user");
    }
    user.value = osmUser;
    isLoggedIn.value = true;
  }

  function logout(): void {
    logoutUser();
    user.value = emptyOsmUser;
    isLoggedIn.value = false;
    window.location.href = "/";
  }

  return {
    isLoggedIn,
    user,
    authenticate,
    setToken,
    loadUser,
    logout,
    isLoggedInCheck,
    initAuth,
  };
});
